//
//  NewsUIController.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit

final class NewsUIController {
    
    private unowned var view: UIView
    private let tableViewDataSource: TableViewDataSource<NewsCellController, NewsTableViewCell>
    
    var state: UIState = .loading {
        willSet(newState) {
            update(newState: newState)
        }
    }
    
    init(view: UIView, tableView: UITableView) {
        self.view = view
        self.tableViewDataSource = TableViewDataSource<NewsCellController, NewsTableViewCell>(tableView: tableView)
        tableView.dataSource = tableViewDataSource
    }
}

extension NewsUIController: NewsDelegate {
    func update(newState: UIState) {
        switch (state, newState) {
        case (.loading, .loading): loadingToLoading()
        case (.loading, .success(let articles)): loadingToSuccess(articles: articles)
        default: fatalError("Not yet implemented \(state) to \(newState)")
        }
    }
    
    func loadingToLoading() {
        LoadingIndicator.show()
    }
    
    func loadingToSuccess(articles: [ArticlesMappable]) {
        LoadingIndicator.hide()
        tableViewDataSource.dataSource = articles.map(NewsCellController.init)
    }
}
