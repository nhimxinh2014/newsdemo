//
//  NewsViewController.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    
    private let newsHandler: NewsHandler
    private var newsUIController: NewsUIController!
    private let tableView = UITableView(frame: .zero)

    init(newsHandler: NewsHandler) {
        self.newsHandler = newsHandler
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        newsHandler.delegate = newsUIController
        newsHandler.fetchNews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    private func setupView() {
        title = "TechCrunch"
        view.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 90.0
        tableView.rowHeight = UITableView.automaticDimension
        newsUIController = NewsUIController(view: view, tableView: tableView)
    }
}
