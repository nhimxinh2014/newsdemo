//
//  CellConfigurable.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

protocol CellConfigurable: class {
    
    associatedtype Controller
    var cellController: Controller? { get set }
}
