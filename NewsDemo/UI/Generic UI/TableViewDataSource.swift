//
//  TableViewDataSource.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit

final class TableViewDataSource<Model, Cell: UITableViewCell>: NSObject, UITableViewDataSource where Cell: CellConfigurable, Model == Cell.Controller {
    
    var dataSource: [Model] = [] {
        didSet { tableView.reloadData() }
    }
    
    private unowned var tableView: UITableView
    
    init(tableView: UITableView) {
        self.tableView = tableView
        
        tableView.registerCell(type: Cell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell: Cell = tableView.dequeueReusableCell(type: Cell.self) else { return UITableViewCell()}
        cell.cellController = dataSource[indexPath.row]
        return cell
    }
}

protocol CellConfigurable: class {
    
    associatedtype Controller
    var cellController: Controller? { get set }
}
