//
//  TableViewDequeuing.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerCell<T: UITableViewCell>(type: T.Type) {
        self.register(nibFromClass(type), forCellReuseIdentifier: nameOfClass(type))
    }
    
    func dequeueReusableCell<T:UITableViewCell>(type: T.Type) -> T? {
        return self.dequeueReusableCell(withIdentifier: nameOfClass(type)) as? T
    }
    
    func dequeueReusableCell<T:UITableViewCell>(type: T.Type, indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withIdentifier: nameOfClass(type), for: indexPath) as? T
    }
}

func nibFromClass<T: NSObject>(_ className: T.Type) -> UINib? {
    let name = String(describing: T.self)
    if Bundle.main.path(forResource: name, ofType: "nib") != nil || Bundle.main.path(forResource: name, ofType: "xib") != nil {
        return UINib(nibName: String(describing: T.self), bundle: nil)
    }
    return nil
}

func nameOfClass<T: NSObject>(_ type: T.Type) -> String {
    return String(describing: T.self)
}
