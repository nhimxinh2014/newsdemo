//
//  LoadingView.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit

final class LoadingIndicator {
    
    static var currentOverlay: UIView?
    
    static func show() {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow)
    }
    
    static func show(_ loadingText: String) {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow, loadingText: loadingText)
    }
    
    static func show(_ overlayTarget: UIView) {
        show(overlayTarget, loadingText: nil)
    }
    
    static func show(_ overlayTarget: UIView, loadingText: String?) {
        DispatchQueue.main.async {
            hide()
            let overlay = UIView(frame: overlayTarget.frame)
            overlay.center = overlayTarget.center
            overlay.alpha = 0.0
            overlay.backgroundColor = .black
            overlayTarget.addSubview(overlay)
            overlayTarget.bringSubviewToFront(overlay)
            let indicator = UIActivityIndicatorView(style: .whiteLarge)
            indicator.color = .white
            indicator.center = overlay.center
            indicator.startAnimating()
            overlay.addSubview(indicator)
            if let textString = loadingText {
                let label = UILabel()
                label.text = textString
                label.textColor = .white
                label.sizeToFit()
                label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30.0)
                overlay.addSubview(label)
            }
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.5)
            overlay.alpha = overlay.alpha > 0.0 ? 0.0 : 0.4
            UIView.commitAnimations()
            currentOverlay = overlay
        }
    }
    
    static func hide() {
        guard let overlay = currentOverlay else {
            return
        }
        DispatchQueue.main.async {
            overlay.removeFromSuperview()
            currentOverlay = nil
        }
    }
    
}
