//
//  AppDelegate.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setRootViewController()
        return true
    }

    private func setRootViewController() {
        let newsViewController = NewsViewController(newsHandler: NewsController())
        let navigationController = UINavigationController(rootViewController: newsViewController)
        navigationController.navigationBar.isTranslucent = false
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

