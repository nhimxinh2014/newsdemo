//
//  NewsCellController.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

protocol NewsCellRepresentable {
    
    var acticle: ArticlesMappable { get }
}

struct NewsCellController: NewsCellRepresentable {
    
    let acticle: ArticlesMappable
    
    init(acticle: ArticlesMappable) {
        self.acticle = acticle
    }
}
