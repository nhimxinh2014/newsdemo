//
//  NewsTableViewCell.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import UIKit
import Kingfisher

extension NewsTableViewCell: CellConfigurable {}

class NewsTableViewCell: UITableViewCell {
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var imageNews: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var cellController: NewsCellController? {
        didSet {
            guard let controller = cellController,
                let imageUrl = URL(string: controller.acticle.urlToImage) else { return }
            imageNews.kf.setImage(with: imageUrl)
            authorLabel.text = controller.acticle.author
            titleLabel.text = controller.acticle.title
            descriptionLabel.text = controller.acticle.description
        }
    }
}
