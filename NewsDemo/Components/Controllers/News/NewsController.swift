//
//  NewsController.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import Foundation
import UIKit

enum UIState {
    case loading
    case success([ArticlesMappable])
    case failure(String)
}

protocol NewsDelegate {
    var state: UIState { get set}
}

protocol NewsHandler: class {
    var delegate: NewsDelegate? { get set}
    func fetchNews()
}

final class NewsController: NewsHandler {
        
    var delegate: NewsDelegate?
    
    func fetchNews() {
        delegate?.state = .loading
        NetworkManager.shared.doRequest(router: .getNews) { [weak self] (response) in
            let failureMessage = "Error Message" // Mockup , Remove later
            guard let responseData = response?.responseData as? [String : Any],
                let newsResponse = NewsModel(JSON: responseData) else { return }
            self?.showResponse(response,
                              failureMessage: failureMessage,
                              success: {
                                self?.delegate?.state = .success(newsResponse.articles)
            }, failure: {
                self?.delegate?.state = .failure(newsResponse.message)
            })
        }
    }
    
    func showResponse(_ response: ResponseObject?, acceptableStatuses: [NetworkManager.HttpStatusCode] = [.success],
                      failureMessage: String?, success: (() -> Void)? = nil, failure: (() -> Void)? = nil) {
        LoadingIndicator.hide()
        let validate = Utility.shared.validate(response: response, failureMessage: failureMessage,
                                               acceptableStatuses: acceptableStatuses)
        if validate {
            success?()
        } else {
            failure?()
        }
    }
}
