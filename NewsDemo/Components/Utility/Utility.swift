//
//  Utility.swift
//  NewsDemo
//
//  Created by Nguyen Tuan Hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import Foundation
import UIKit

class Utility: NSObject {
    
    static let shared = Utility()
    
    func showMessage(inViewController viewControllerShow: UIViewController? = nil,
                     title: String? = nil, message: String, okButtonTitle: String = "Ok",
                     completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okButtonTitle, style: .default, handler: { (_) in
            completion?()
        }))
        let viewController = getPresentableViewController(by: viewControllerShow)
        DispatchQueue.main.async {
            viewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNetworkError(inViewController viewControllerShow: UIViewController? = nil) {
        let message = NetworkManager.isReachable ? "Error System" : "Error API"
        let viewController = getPresentableViewController(by: viewControllerShow)
        showMessage(inViewController: viewController, message: message, completion: nil)
    }
    
    func validate(response: ResponseObject?, failureMessage: String?,
                  acceptableStatuses: [NetworkManager.HttpStatusCode]) -> Bool {
        LoadingIndicator.hide()
        let visibleViewController = getVisibleViewControllerOfWindow()
        guard let response = response, response.isSuccess == true else {
            showNetworkError(inViewController: visibleViewController)
            return false
        }
        if !acceptableStatuses.contains(response.httpStatusCode), let failureMessage = failureMessage {
            showMessage(inViewController: visibleViewController, message: failureMessage, completion: nil)
            return false
        }
        return true
    }
    
    func getPresentableViewController(by viewController: UIViewController?) -> UIViewController? {
        let viewControllerReturn: UIViewController?
        if let viewController = viewController {
            viewControllerReturn = getVisibleViewController(from: viewController)
        } else {
            viewControllerReturn = getVisibleViewControllerOfWindow()
        }
        return viewControllerReturn
    }
    
    func getVisibleViewControllerOfWindow() -> UIViewController? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        guard let viewController = appDelegate?.window?.rootViewController else {
            return nil
        }
        return getVisibleViewController(from: viewController)
    }
    
    func getVisibleViewController(from viewController: UIViewController?) -> UIViewController? {
        if let navigationViewController = viewController as? UINavigationController {
            return getVisibleViewController(from: navigationViewController.visibleViewController)
        } else if let tabbarViewController = viewController as? UITabBarController {
            return getVisibleViewController(from: tabbarViewController.selectedViewController)
        } else {
            if let presentedViewController = viewController?.presentedViewController {
                return getVisibleViewController(from: presentedViewController)
            } else {
                return viewController
            }
        }
    }
    
    func replaceRootViewController(by viewController: UIViewController?) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        appdelegate?.window?.rootViewController = viewController
    }
}

