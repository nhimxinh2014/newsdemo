//
//  BaseModel.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import ObjectMapper

class BaseMappable: Mappable {
    var status = String()
    var code = String()
    var message = String()

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        status <- map["status"]
        code <- map["code"]
        message <- map["message"]
    }
}
