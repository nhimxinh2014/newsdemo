//
//  NewsModel.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import ObjectMapper

class NewsModel: BaseMappable {
    var totalResults: Int?
    var articles = [ArticlesMappable]()

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)
        totalResults <- map["totalResults"]
        articles <- map["articles"]
    }
}
