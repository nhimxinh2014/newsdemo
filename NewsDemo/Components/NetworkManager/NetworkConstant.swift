//
//  NetworkConstant.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import Foundation

struct NetworkConstant {

    // TODOs: Config Environment
    static let baseURLString = "https://newsapi.org/"
    static let versionApi = "v2/"
    static let apiKey = "42616655bdb440c9964f509ece7ac432"
    static let sources = "techcrunch"
}

struct K {
    static let sources = "sources"
    static let apiKey = "apiKey"
    static let articles = "articles"
}

enum StatusReponse: String {
    case success = "ok"
    case error = "error"
}
