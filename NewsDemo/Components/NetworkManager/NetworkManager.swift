//
//  NetworkManager.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import Foundation
import Alamofire

typealias ResponseCompletion = ((_ response: ResponseObject?) -> Void)

class NetworkManager: SessionManager {

    static let shared = NetworkManager()

    enum HttpStatusCode: Int {

        case success = 200
        case forbidden = 403
        case notFound = 404
        case serverError = 500

    }

    private let networkReachability = NetworkReachabilityManager()
    static private(set) var isReachable = true

    fileprivate init() {
        super.init(configuration: .default)
        networkReachability?.startListening()
        networkReachability?.listener = { [weak self] status in
            // TODO: Internet connection change
            NetworkManager.isReachable = self?.networkReachability?.isReachable ?? false
        }
    }

    func cancelAllRequest() {
        session.getAllTasks { (tasks) in
            tasks.forEach({ $0.cancel() })
        }
    }

    func doRequest(router: Router, completion: ResponseCompletion?) {
        let statusCodeValids: [HttpStatusCode] = [.success, .notFound]
        let networkRequest = request(router).validate(statusCode: statusCodeValids.map({ $0.rawValue }))
        networkRequest.responseJSON { (responseData: DataResponse<Any>) in
            let response = ResponseObject()
            if let code = responseData.response?.statusCode, let httpStatusCode = HttpStatusCode(rawValue: code) {
                response.httpStatusCode = httpStatusCode
            } else {
                response.httpStatusCode = .success
            }
            response.responseData = responseData.result.value
            response.error = responseData.result.error
            completion?(response)
        }
    }

    func getNewsTechCrunch(completion: ResponseCompletion?) {
        doRequest(router: .getNews, completion: completion)
    }
}
