//
//  Router.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {

    case getNews

   private var method: HTTPMethod {
        switch self {
        case .getNews:
            return .get
        }
    }

    private var path: String {
        switch self {
        case .getNews:
            return "top-headlines"
        }
    }

    internal func asURLRequest() throws -> URLRequest {
        var url = try NetworkConstant.baseURLString.asURL()
        url.appendPathComponent(NetworkConstant.versionApi)
        url.appendPathComponent(path)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        let parameters: Parameters = [ // var if append key and value
            K.apiKey : NetworkConstant.apiKey,
            K.sources : NetworkConstant.sources
        ]

        switch self {
        case .getNews:
           urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }
}
