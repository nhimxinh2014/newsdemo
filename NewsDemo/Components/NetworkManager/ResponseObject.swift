//
//  ResponseObject.swift
//  NewsDemo
//
//  Created by nguyen.tuan.hai on 4/3/19.
//  Copyright © 2019 Nguyen Tuan Hai. All rights reserved.
//

import Foundation

class ResponseObject: NSObject {
    var httpStatusCode: NetworkManager.HttpStatusCode = .success
    var responseData: Any?
    var error: Error?
    var isSuccess: Bool {
        return error == nil
    }
}
